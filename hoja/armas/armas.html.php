<div class="card col pt-4 pb-4">
	<div class="row">
		<div class="input-group mb-2 col-12 col-sm-6">
			<div class="input-group-prepend"><span class="input-group-text" id="agilidad-lab">Modificador Ataque</span></div>
			<input type="number" class="form-control" id="modAta" name="armasModAta" value="<?= $armas['modificadorAtaque']; ?>" max="999">
		</div>
		<div class="input-group mb-2 col-12 col-sm-6">
			<div class="input-group-prepend"><span class="input-group-text" id="agilidad-lab">Modificador Defensa</span></div>
			<input type="number" class="form-control" id="modDef" name="armasModDef" value="<?= $armas['modificadorDefensa']; ?>" max="999">
		</div>
	</div>
	<div class="row p-2" id="mele">
		<div class="col-12"><hr></div>
		<div class="col-12 text-center mb-4"><span class="h4">Cuerpo a cuerpo</span></div>
		<div class="d-sm-none" id="isMovile"></div>
		<?php foreach($listaArmasMele as $arma): ?>
			<div class="card col-12 col-sm-6 position-relative mb-2">
				<i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarArma(this)"></i>
				<div class="card-body pl-1 pr-1">
					<div class="row">
						<input type="text" name="armaId[]" value="<?= $arma['id']; ?>" class="d-none">
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="arma-nombre-lab">Arma</span></div>
									<input type="text" class="form-control" name="armaNombre[]" value="<?= $arma['arma']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="arma-danyo-lab">Daño</span></div>
									<input type="text" class="form-control" name="armaDanyo[]" value="<?= $arma['danyo']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pr-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="arma-mr-lab">MR</span></div>
									<input type="number" class="form-control" name="armaMR[]" value="<?= $arma['movimientoReaccion']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pl-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="arma-%a-lab">A%</span></div>
									<input type="number" class="form-control" name="armaPorAta[]" value="<?= $arma['ataque']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pr-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="arma-%d-lab">D%</span></div>
									<input type="number" class="form-control" name="armaPorDef[]" value="<?= $arma['defensa']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pl-1">
							<div class="row">
								<div class="col input-group">	
									<div class="input-group-prepend"><span class="input-group-text" id="arma-pa-lab">PA</span></div>
									<input type="number" class="form-control" name="armaPA[]" value="<?= $arma['puntosAguante']; ?>" max="999">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="row justify-content-center mt-2">
		<div class="col-1 p-0 text-center">
			<i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addMele()"></i>
		</div>
	</div>
	<div class="row p-2" id="distancia">
		<div class="col-12"><hr></div>
		<div class="col-12 text-center mb-4"><span class="h4">A distancia</span></div>
		<?php foreach($listaArmasDistancia as $armaDistancia): ?>
			<div class="card col-12 col-sm-6 position-relative mb-2">
				<i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarArmaDistancia(this)"></i>
				<div class="card-body pl-1 pr-1">
					<div class="row">
						<input type="text" name="armaDistanciaId[]" value="<?= $armaDistancia['id']; ?>" class="d-none">
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-nombre-lab">Arma</span></div>
									<input type="text" class="form-control" name="armaDistanciaNombre[]" value="<?= $armaDistancia['arma']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-danyo-lab">Daño</span></div>
									<input type="text" class="form-control" name="armaDistanciaDanyo[]" value="<?= $armaDistancia['danyo']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-mr-lab">Tasa fuego</span></div>
									<input type="number" class="form-control" name="armaDistanciaTasaFuego[]" value="<?= $armaDistancia['tasaFuego']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">	
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-pa-lab">Alcance</span></div>
									<input type="text" class="form-control" name="armaDistanciaAlcance[]" value="<?= $armaDistancia['alcance']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pl-sm-1 offset-sm-3">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-%a-lab">A%</span></div>
									<input type="number" class="form-control" name="armaDistanciaPorAta[]" value="<?= $armaDistancia['ataque']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pr-sm-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-%d-lab">D%</span></div>
									<input type="number" class="form-control" name="armaDistanciaPorDef[]" value="<?= $armaDistancia['defensa']; ?>" max="999">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="row justify-content-center mt-2">
		<div class="col-1 p-0 text-center">
			<i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addDistancia()"></i>
		</div>
	</div>
	<div class="row p-2" id="escudo">
		<div class="col-12"><hr></div>
		<div class="col-12 text-center mb-4"><span class="h4">Escudos</span></div>
		<?php foreach($listaArmasEscudos as $escudo): ?>
			<div class="card col-12 col-sm-6 position-relative mb-2">
				<i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarEscudo(this)"></i>
				<div class="card-body pl-1 pr-1">
					<div class="row">
						<input type="text" name="escudoId[]" value="<?= $escudo['id']; ?>" class="d-none">
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-nombre-lab">Escudo</span></div>
									<input type="text" class="form-control" name="escudoNombre[]" value="<?= $escudo['arma']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-12 pb-2">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-danyo-lab">Daño</span></div>
									<input type="text" class="form-control" name="escudoDanyo[]" value="<?= $escudo['danyo']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pr-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-mr-lab">MR</span></div>
									<input type="number" class="form-control" name="escudoMR[]" value="<?= $escudo['movimientoReaccion']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pl-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-%a-lab">A%</span></div>
									<input type="number" class="form-control" name="escudoPorAta[]" value="<?= $escudo['ataque']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pr-1">
							<div class="row">
								<div class="col input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-%d-lab">D%</span></div>
									<input type="number" class="form-control" name="escudoPorDef[]" value="<?= $escudo['defensa']; ?>" max="999">
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-6 pb-2 pl-1">
							<div class="row">
								<div class="col input-group">	
									<div class="input-group-prepend"><span class="input-group-text" id="escudo-pa-lab">PA</span></div>
									<input type="number" class="form-control" name="escudoPA[]" value="<?= $escudo['puntosAguante']; ?>" max="999">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="row justify-content-center mt-2">
		<div class="col-1 p-0 text-center">
			<i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addEscudo()"></i>
		</div>
	</div>

</div>

<script>
	function borrarArma(obj) {
		var card = $(obj).parent();
		var nombre = card.find('[name="armaNombre[]"]').first();
		if (!confirm("¿Desea borrar el arma: "+nombre.val()+"?")) { return; }
		card.addClass("d-none");
		nombre.val("-99999");
	}
	
	function borrarArmaDistancia(obj) {
		var card = $(obj).parent();
		var nombre = card.find('[name="armaDistanciaNombre[]"]').first();
		if (!confirm("¿Desea borrar el arma: "+nombre.val()+"?")) { return; }
		card.addClass("d-none");
		nombre.val("-99999");
	}
	
	function borrarEscudo(obj) {
		var card = $(obj).parent();
		var nombre = card.find('[name="escudoNombre[]"]').first();
		if (!confirm("¿Desea borrar el escudo: "+nombre.val()+"?")) { return; }
		card.addClass("d-none");
		nombre.val("-99999");
	}

	function borrarArmaNoEnBD(obj) {
		if (!confirm("¿Desea borrar el arma: "+$(obj).parent().find('[name="armaNombre[]"]').first().val()+"?")) { return; }
		$(obj).parent().remove();
	};
	
	function borrarArmaDistanciaNoEnBD(obj) {
		if (!confirm("¿Desea borrar el arma: "+$(obj).parent().find('[name="armaDistanciaNombre[]"]').first().val()+"?")) { return; }
		$(obj).parent().remove();
	};
	
	function borrarEscudoNoEnBD(obj) {
		if (!confirm("¿Desea borrar el escudo: "+$(obj).parent().find('[name="escudoNombre[]"]').first().val()+"?")) { return; }
		$(obj).parent().remove();
	};

	//scroll para que se vea el nuevo campo correctamente
	function scrollNuevoCampoArmas() {
		var y = $(window).scrollTop();  //your current y position on the page
		$(window).scrollTop(y+138);
	}

	function scrollNuevoCampoArmasMovil() {
		var y = $(window).scrollTop();
		$(window).scrollTop(y+234);
	}

	function isMovile() {
		return !($("#isMovile").css('display') == 'none');
	}
	
	function addMele() {
		$("#mele").append('<div class="card col-12 col-sm-6 position-relative mb-2"><i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarArmaNoEnBD(this)"></i><div class="card-body pl-1 pr-1"><div class="row"><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-nombre-lab">Arma</span></div><input type="text" class="form-control" name="addArmaNombre[]"></div></div></div><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-danyo-lab">Daño</span></div><input type="text" class="form-control" name="addArmaDanyo[]"></div></div></div><div class="col-sm-3 col-6 pb-2 pr-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-mr-lab">MR</span></div><input type="number" class="form-control" name="addArmaMR[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pl-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-%a-lab">A%</span></div><input type="number" class="form-control" name="addArmaPorAta[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pr-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-%d-lab">D%</span></div><input type="number" class="form-control" name="addArmaPorDef[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pl-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-pa-lab">PA</span></div><input type="number" class="form-control" name="addArmaPA[]" max="999"></div></div></div></div></div></div>');
		if (!isMovile() && ($("#mele").children(".card").length % 2) == 1) {
			scrollNuevoCampoArmas();
		} else if (isMovile()) {
			scrollNuevoCampoArmasMovil();
		}
	}

	function addEscudo() {
		$("#escudo").append('<div class="card col-12 col-sm-6 position-relative mb-2"><i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarEscudoNoEnBD(this)"></i><div class="card-body pl-1 pr-1"><div class="row"><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-nombre-lab">Escudo</span></div><input type="text" class="form-control" name="addEscudoNombre[]"></div></div></div><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-danyo-lab">Daño</span></div><input type="text" class="form-control" name="addEscudoDanyo[]"></div></div></div><div class="col-sm-3 col-6 pb-2 pr-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-mr-lab">MR</span></div><input type="number" class="form-control" name="addEscudoMR[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pl-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-%a-lab">A%</span></div><input type="number" class="form-control" name="addEscudoPorAta[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pr-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-%d-lab">D%</span></div><input type="number" class="form-control" name="addEscudoPorDef[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pl-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="arma-pa-lab">PA</span></div><input type="number" class="form-control" name="addEscudoPA[]" max="999"></div></div></div></div></div></div>');
		if (!isMovile() && ($("#escudo").children(".card").length % 2) == 1) {
			scrollNuevoCampoArmas();
		} else if (isMovile()) {
			scrollNuevoCampoArmasMovil();
		}
	}
	
	function addDistancia() {
		$("#distancia").append('<div class="card col-12 col-sm-6 position-relative mb-2"><i class="fas fa-times rojo p-0 cursorPointer col-1 position-absolute fixed-top ml-auto p-1 text-right" onclick="borrarArmaDistanciaNoEnBD(this)"></i><div class="card-body pl-1 pr-1"><div class="row"><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-nombre-lab">Arma</span></div><input type="text" class="form-control" name="addArmaDistanciaNombre[]"></div></div></div><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-danyo-lab">Daño</span></div><input type="text" class="form-control" name="addArmaDistanciaDanyo[]"></div></div></div><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-mr-lab">Tasa fuego</span></div><input type="number" class="form-control" name="addArmaDistanciaTasaFuego[]" max="999"></div></div></div><div class="col-sm-6 col-12 pb-2"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-pa-lab">Alcance</span></div><input type="text" class="form-control" name="addArmaDistanciaAlcance[]"></div></div></div><div class="col-sm-3 col-6 pb-2 pl-sm-1 offset-sm-3"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-%a-lab">A%</span></div><input type="number" class="form-control" name="addArmaDistanciaPorAta[]" max="999"></div></div></div><div class="col-sm-3 col-6 pb-2 pr-sm-1"><div class="row"><div class="col input-group"><div class="input-group-prepend"><span class="input-group-text" id="armaDistancia-%d-lab">D%</span></div><input type="number" class="form-control" name="addArmaDistanciaPorDef[]" max="999"></div></div></div></div></div></div>');
		if (!isMovile() && ($("#distancia").children(".card").length % 2) == 1) {
			scrollNuevoCampoArmas();
		} else if (isMovile()) {
			scrollNuevoCampoArmasMovil();
		}
	}
</script>