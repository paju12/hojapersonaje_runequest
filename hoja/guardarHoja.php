<?php 
require('../base/functions.php');
require('../base/checkLogin.php');

//pasar a null los campos vacios
foreach ($_POST as $key => $value) {
	if ($value == "0") { continue; }
	if (empty($value)) $_POST[$key] = null; 
}
// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";


function  insertIntoListaExtra($tabla, $nombreListaNombres, $nombreListaValores, $id, $conn) {
	if (!isset($_POST[$nombreListaNombres])) { return; }
	for ($i=0; $i < count($_POST[$nombreListaNombres]) ; $i++) { 
		$sql = "INSERT INTO ".$tabla." values(?,?,?,?)";
		prepared_query($conn, $sql, [null, $id, $_POST[$nombreListaNombres][$i], $_POST[$nombreListaValores][$i]])->close(); 
	}
}

function  updateIntoListaExtra($tabla, $nombreListaNombres, $nombreListaValores, $nombreListaIds, $id, $conn) {
	for ($i=0; $i < count($_POST[$nombreListaNombres]) ; $i++) {
		$idHabilidad = $_POST[$nombreListaIds][$i];
		$nombre = $_POST[$nombreListaNombres][$i];
		$val = $_POST[$nombreListaValores][$i];
		if ($val == "-99999") {
			$sql = "DELETE FROM ".$tabla." WHERE id = ?";
			prepared_query($conn, $sql, [$idHabilidad])->close(); 
		} else {
			$sql = "UPDATE ".$tabla." SET nombreHabilidad=?, valorHabilidad=? WHERE id = ?";
			if ($tabla == "listaExtraMagia") {
				$sql = "UPDATE ".$tabla." SET nombreMagia=?, valorMagia=? WHERE id = ?";
			}
			prepared_query($conn, $sql, [$nombre, $val, $idHabilidad])->close(); 
		}
	}
}

function  insertIntoArmas($tabla, $nombreListaNombres, $nombreListaDanyo, $nombreListaMr, $nombreListaAtaque, $nombreListaDefensa, $nombreListaPa, $id, $conn) {
	if (!isset($_POST[$nombreListaNombres])) { return; }
	for ($i=0; $i < count($_POST[$nombreListaNombres]) ; $i++) { 
		$sql = "INSERT INTO ".$tabla." values(?,?,?,?,?,?,?,?)";
		prepared_query($conn, $sql, [null, $id,
			$_POST[$nombreListaNombres][$i], $_POST[$nombreListaDanyo][$i],
			$_POST[$nombreListaMr][$i], $_POST[$nombreListaAtaque][$i],
			$_POST[$nombreListaDefensa][$i], $_POST[$nombreListaPa][$i]
			])->close(); 
	}
}

function  updateIntoArmas($tabla, $nombreListaNombres, $nombreListaDanyo, $nombreListaMr, $nombreListaAtaque, $nombreListaDefensa, $nombreListaPa, $nombreListaIds, $id, $conn) {
	for ($i=0; $i < count($_POST[$nombreListaNombres]) ; $i++) {
		$id = $_POST[$nombreListaIds][$i];
		$val = $_POST[$nombreListaNombres][$i];
		if ($val == "-99999") {
			$sql = "DELETE FROM ".$tabla." WHERE id = ?";
			prepared_query($conn, $sql, [$id])->close(); 
		} else {
			$sql = "UPDATE ".$tabla." SET arma=?, danyo=?, movimientoReaccion=?, ataque=?, defensa=?, puntosAguante=? WHERE id = ?";
			if ($tabla == "listaArmasDistancia") {
				$sql = "UPDATE ".$tabla." SET arma=?, danyo=?, tasaFuego=?, ataque=?, defensa=?, alcance=? WHERE id = ?";
			}
			prepared_query($conn, $sql, [$_POST[$nombreListaNombres][$i], $_POST[$nombreListaDanyo][$i], $_POST[$nombreListaMr][$i], $_POST[$nombreListaAtaque][$i], $_POST[$nombreListaDefensa][$i], $_POST[$nombreListaPa][$i], $id])->close(); 
		}
	}
}

if ($_POST['modDanyo'] == null) {
	$_POST['modDanyo'] = 0;
}
if ($_POST['mov'] == null) {
	$_POST['mov'] = 0;
}
if ($_POST['mmrDes'] == null) {
	$_POST['mmrDes'] = 0;
}
if ($_POST['mmrTam'] == null) {
	$_POST['mmrTam'] = 0;
}
if ($_POST['mmrCc'] == null) {
	$_POST['mmrCc'] = 0;
}

$conn = dbCon();
if (empty($_POST['id'])) {
	$sql = "INSERT INTO `hojas`(`id`,`propietario`, `nombreAventurero`, `nombreJugador`, `edad`, `genero`, `especie`, `cultura`, `clanProcedencia`, `profesionPadres`, `profesionAventurero`, `religion`, `carAct_fue`, `carAct_con`, `carAct_tam`, `carAct_int`, `carAct_per`, `carAct_des`, `carAct_asp`, `carOri_fue`, `carOri_con`, `carOri_tam`, `carOri_int`, `carOri_per`, `carOri_des`, `carOri_asp`, `atr_modDanyo`, `atr_mov`, `atr_mmr-des`, `atr_mmr-tam`, `atr_mmr-cc`, `textoLibre`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
	$q = prepared_query($conn, $sql, [
		null,
		getUsuFromCoockieSesion(),
		$_POST['nombreAventurero'],
		$_POST['nombreJugador'],
		$_POST['edad'],
		$_POST['genero'],
		$_POST['especie'],
		$_POST['cultura'],
		$_POST['clan'],
		$_POST['profesionPadres'],
		$_POST['profesionAventurero'],
		$_POST['religion'],
		$_POST['fueAct'],
		$_POST['conAct'],
		$_POST['tamAct'],
		$_POST['iniAct'],
		$_POST['perAct'],
		$_POST['desAct'],
		$_POST['aspAct'],
		$_POST['fueOri'],
		$_POST['conOri'],
		$_POST['tamOri'],
		$_POST['intOri'],
		$_POST['perOri'],
		$_POST['desOri'],
		$_POST['aspOri'],
		$_POST['modDanyo'],
		$_POST['mov'],
		$_POST['mmrDes'],
		$_POST['mmrTam'],
		$_POST['mmrCc'],
		$_POST['textoLibre']
		], "isssissssssssssssssssssssssiiiis");

	$q->close();

	$stmt = prepared_query($conn, "SELECT id FROM hojas WHERE propietario=? AND nombreAventurero=? ORDER BY id DESC LIMIT 1", [getUsuFromCoockieSesion(),$_POST['nombreAventurero']], "ss");
	$hojaNueva = $stmt->get_result()->fetch_assoc();
	$stmt->close();

	// $id = obtener el id por sql del insert anterior
	$id = $hojaNueva['id'];
	
	//Equipo
	insertIntoListaExtra("listaEquipo", "addEquipoNombre", "addEquipoCar", $id, $conn);

	//habilidades agilidad
	$sql = "INSERT INTO habilidadesAgilidad(hojaRelacionada, bonificador, arrojar, esquivar, montar, nadar, remar, saltar, trepar) VALUES (?,?,?,?,?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habAgiBonificador'],
		$_POST['habAgiArrojar'],
		$_POST['habAgiEsquivar'],
		$_POST['habAgiMontar'],
		$_POST['habAgiNadar'],
		$_POST['habAgiRemar'],
		$_POST['habAgiSaltar'],
		$_POST['habAgiTrepar']
		], "")->close();

	insertIntoListaExtra("listaExtraAgilidad", "addAgilidadNombre", "addAgilidadValor", $id, $conn);

	//habilidades comunicación
	$sql = "INSERT INTO habilidadesComunicacion(hojaRelacionada, bonificador, cantar, hablaFluida, oratoria) VALUES (?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habComBonificador'],
		$_POST['habComCantar'],
		$_POST['habComHablaFluida'],
		$_POST['habComOratoria']
	], "iiiii")->close();
	
	insertIntoListaExtra("listaExtraComunicacion", "addComunicacionIdiomasNombre", "addComunicacionIdiomasValor", $id, $conn);

	//habilidades conocimiento
	$sql = "INSERT INTO `habilidadesConocimiento`(`hojaRelacionada`, `bonificador`, `marciales`, `animal`, `mundo`, `humano`, `mineral`, `vegetal`, `navegacion`, `1auxilios`, `valorarObjetos`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habConBonificador'],
		$_POST['habConMar'],
		$_POST['habConAnimal'],
		$_POST['habConMundo'],
		$_POST['habConHumano'],
		$_POST['habConMineral'],
		$_POST['habConVegetal'],
		$_POST['habConNavegacion'],
		$_POST['habCon1Auxilios'],
		$_POST['habConValorarObjetos']
	], "")->close();

	insertIntoListaExtra("listaExtraConocimientoFabricacion", "addConocimientoFabNombre", "addConocimientoFabValor", $id, $conn);
	insertIntoListaExtra("listaExtraConocimientoLeer", "addConocimientoIdiNombre", "addConocimientoIdiValor", $id, $conn);

	//Habilidades manipulacion
	$sql = "INSERT INTO `habilidadesManipulacion`(`hojaRelacionada`, `bonificador`, `inventar`, `ocultar`, `trucos`) VALUES (?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habManBonificador'],
		$_POST['habManInv'],
		$_POST['habManOcu'],
		$_POST['habManTru']
	], "")->close();

	insertIntoListaExtra("listaExtraManipulacion", "addManipulacionNombre", "addManipulacionValor", $id, $conn);

	//Habilidades percepcion
	$sql = "INSERT INTO `habilidadesPercepcion`(`hojaRelacionada`, `bonificador`, `buscar`, `escuchar`, `otear`, `rastrear`) VALUES (?,?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habPerBonificador'],
		$_POST['habPerBus'],
		$_POST['habPerEsc'],
		$_POST['habPerOte'],
		$_POST['habPerRas']
	], "")->close();
	
	//Habilidades sigilo
	$sql = "INSERT INTO `habilidadesSigilo`(`hojaRelacionada`, `bonificador`, `deslizarse`, `esconderse`) VALUES (?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['habSigBonificador'],
		$_POST['habSigDes'],
		$_POST['habSigEsc']
	], "")->close();

	//Magia
	$sql = "INSERT INTO magia(`hojaRelacionada`, `bonificador`, `ceremonia`, `encantamiento`, `invocacion`, `duracion`, `intensidad`, `multiconjuro`, `alcance`) VALUES (?,?,?,?,?,?,?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['magiaBonificador'],
		$_POST['magiaCeremonia'],
		$_POST['magiaEncantamiento'],
		$_POST['magiaInvocacion'],
		$_POST['magiaDuracion'],
		$_POST['magiaIntensidad'],
		$_POST['magiaMulticonjuro'],
		$_POST['magiaAlcance']
		], "")->close();

	insertIntoListaExtra("listaExtraMagia", "addMagiaNombre", "addMagiaValor", $id, $conn);
	
	$sql = "INSERT INTO armas(`hojaRelacionada`, `modificadorAtaque`, `modificadorDefensa`) VALUES (?,?,?)";
	prepared_query($conn, $sql, [
		$id,
		$_POST['armasModAta'],
		$_POST['armasModDef']
	])->close();

	//armas mele
	insertIntoArmas("listaArmasMele", "addArmaNombre", "addArmaDanyo", "addArmaMR", "addArmaPorAta", "addArmaPorDef", "addArmaPA", $id, $conn);

	//armas escudo
	insertIntoArmas("listaArmasEscudos", "addEscudoNombre", "addEscudoDanyo", "addEscudoMR", "addEscudoPorAta", "addEscudoPorDef", "addEscudoPA", $id, $conn);

	//armas distancia
	insertIntoArmas("listaArmasDistancia", "addArmaDistanciaNombre", "addArmaDistanciaDanyo", "addArmaDistanciaTasaFuego", "addArmaDistanciaPorAta", "addArmaDistanciaPorDef", "addDistanciaArmaAlcance", $id, $conn);

} else {
	$id = $_POST['id'];
	$sql = "UPDATE `hojas` SET `nombreAventurero`=?,`nombreJugador`=?,`edad`=?,`genero`=?,`especie`=?,`cultura`=?,`clanProcedencia`=?,`profesionPadres`=?,`profesionAventurero`=?,`religion`=?,`carAct_fue`=?,`carAct_con`=?,`carAct_tam`=?,`carAct_int`=?,`carAct_per`=?,`carAct_des`=?,`carAct_asp`=?,`carOri_fue`=?,`carOri_con`=?,`carOri_tam`=?,`carOri_int`=?,`carOri_per`=?,`carOri_des`=?,`carOri_asp`=?,`atr_modDanyo`=?,`atr_mov`=?,`atr_mmr-des`=?,`atr_mmr-tam`=?,`atr_mmr-cc`=?, `textoLibre`=? WHERE id = ? AND propietario=?";
	
	prepared_query($conn, $sql, [
		$_POST['nombreAventurero'],
		$_POST['nombreJugador'],
		$_POST['edad'],
		$_POST['genero'],
		$_POST['especie'],
		$_POST['cultura'],
		$_POST['clan'],
		$_POST['profesionPadres'],
		$_POST['profesionAventurero'],
		$_POST['religion'],
		$_POST['fueAct'],
		$_POST['conAct'],
		$_POST['tamAct'],
		$_POST['iniAct'],
		$_POST['perAct'],
		$_POST['desAct'],
		$_POST['aspAct'],
		$_POST['fueOri'],
		$_POST['conOri'],
		$_POST['tamOri'],
		$_POST['intOri'],
		$_POST['perOri'],
		$_POST['desOri'],
		$_POST['aspOri'],
		$_POST['modDanyo'],
		$_POST['mov'],
		$_POST['mmrDes'],
		$_POST['mmrTam'],
		$_POST['mmrCc'],
		$_POST['textoLibre'],
		$_POST['id'],
		getUsuFromCoockieSesion()
	])->close();

	//equipo
	insertIntoListaExtra("listaEquipo", "addEquipoNombre", "addEquipoCar", $id, $conn);

	for ($i=0; $i < count($_POST['equipoNombre']) ; $i++) { 
		$idEquipo = $_POST['equipoId'][$i];
		$nombreEquipo = $_POST['equipoNombre'][$i];
		$carEquipo = $_POST['equipoCar'][$i];
		if ($nombreEquipo == "-99999") {
			$sql = "DELETE FROM listaEquipo WHERE id = ?";
			prepared_query($conn, $sql, [$idEquipo])->close(); 
		} else {
			$sql = "UPDATE listaEquipo SET equipo=?, car=? WHERE id = ?";
			prepared_query($conn, $sql, [$nombreEquipo, $carEquipo, $idEquipo])->close(); 
		}
	}

	//habilidades agilidad
	$sql = "UPDATE `habilidadesAgilidad` SET `bonificador`=?,`arrojar`=?,`esquivar`=?,`montar`=?,`nadar`=?,`remar`=?,`saltar`=?,`trepar`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habAgiBonificador'],
		$_POST['habAgiArrojar'],
		$_POST['habAgiEsquivar'],
		$_POST['habAgiMontar'],
		$_POST['habAgiNadar'],
		$_POST['habAgiRemar'],
		$_POST['habAgiSaltar'],
		$_POST['habAgiTrepar'],
		$id
	])->close();
		
	insertIntoListaExtra("listaExtraAgilidad", "addAgilidadNombre", "addAgilidadValor", $id, $conn);
	updateIntoListaExtra("listaExtraAgilidad", "agilidadNombre", "agilidadValor", "agilidadId", $id, $conn);

	//habilidades comunicación
	$sql = "UPDATE `habilidadesComunicacion` SET `bonificador`=?,`cantar`=?,`hablaFluida`=?,`oratoria`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habComBonificador'],
		$_POST['habComCantar'],
		$_POST['habComHablaFluida'],
		$_POST['habComOratoria'],
		$id
	], "iiiii")->close();
	
	insertIntoListaExtra("listaExtraComunicacion", "addComunicacionIdiomasNombre", "addComunicacionIdiomasValor", $id, $conn);
	updateIntoListaExtra("listaExtraComunicacion", "comunicacionIdiomasNombre", "comunicacionIdiomasValor", "comunicacionIdiomasId", $id, $conn);


	//habilidades conocimiento
	$sql = "UPDATE `habilidadesConocimiento` SET `bonificador`=?,`marciales`=?,`animal`=?,`mundo`=?,`humano`=?,`mineral`=?,`vegetal`=?,`navegacion`=?,`1auxilios`=?,`valorarObjetos`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habConBonificador'],
		$_POST['habConMar'],
		$_POST['habConAnimal'],
		$_POST['habConMundo'],
		$_POST['habConHumano'],
		$_POST['habConMineral'],
		$_POST['habConVegetal'],
		$_POST['habConNavegacion'],
		$_POST['habCon1Auxilios'],
		$_POST['habConValorarObjetos'],
		$id
	], "")->close();

	insertIntoListaExtra("listaExtraConocimientoFabricacion", "addConocimientoFabNombre", "addConocimientoFabValor", $id, $conn);
	insertIntoListaExtra("listaExtraConocimientoLeer", "addConocimientoIdiNombre", "addConocimientoIdiValor", $id, $conn);
	updateIntoListaExtra("listaExtraConocimientoFabricacion", "conocimientoFabricacionNombre", "conocimientoFabricacionValor", "conocimientoFabricacionId", $id, $conn);
	updateIntoListaExtra("listaExtraConocimientoLeer", "conocimientoLeerNombre", "conocimientoLeerValor", "conocimientoLeerId", $id, $conn);

	//Habilidades manipulacion
	$sql = "UPDATE `habilidadesManipulacion` SET `bonificador`=?,`inventar`=?,`ocultar`=?,`trucos`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habManBonificador'],
		$_POST['habManInv'],
		$_POST['habManOcu'],
		$_POST['habManTru'],
		$id
	], "")->close();

	insertIntoListaExtra("listaExtraManipulacion", "addManipulacionNombre", "addManipulacionValor", $id, $conn);
	updateIntoListaExtra("listaExtraManipulacion", "manipulacionNombre", "manipulacionValor", "manipulacionId", $id, $conn);

	//Habilidades percepcion
	$sql = "UPDATE `habilidadesPercepcion` SET `bonificador`=?,`buscar`=?,`escuchar`=?,`otear`=?,`rastrear`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habPerBonificador'],
		$_POST['habPerBus'],
		$_POST['habPerEsc'],
		$_POST['habPerOte'],
		$_POST['habPerRas'],
		$id
	], "")->close();

	//Habilidades sigilo
	$sql = "UPDATE `habilidadesSigilo` SET `bonificador`=?,`deslizarse`=?,`esconderse`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['habSigBonificador'],
		$_POST['habSigDes'],
		$_POST['habSigEsc'],
		$id
	], "")->close();

	//Magia
	$sql = "UPDATE `magia` SET `bonificador`=?,`ceremonia`=?,`encantamiento`=?,`invocacion`=?,`duracion`=?,`intensidad`=?,`multiconjuro`=?,`alcance`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['magiaBonificador'],
		$_POST['magiaCeremonia'],
		$_POST['magiaEncantamiento'],
		$_POST['magiaInvocacion'],
		$_POST['magiaDuracion'],
		$_POST['magiaIntensidad'],
		$_POST['magiaMulticonjuro'],
		$_POST['magiaAlcance'],
		$id
	])->close();
		
	insertIntoListaExtra("listaExtraMagia", "addMagiaNombre", "addMagiaValor", $id, $conn);
	updateIntoListaExtra("listaExtraMagia", "magiaNombre", "magiaValor", "magiaId", $id, $conn);
	
	//Armas
	$sql = "UPDATE `armas` SET `modificadorAtaque`=?,`modificadorDefensa`=? WHERE hojaRelacionada = ?";
	prepared_query($conn, $sql, [
		$_POST['armasModAta'],
		$_POST['armasModDef'],
		$id
	])->close();

	//armas mele
	insertIntoArmas("listaArmasMele", "addArmaNombre", "addArmaDanyo", "addArmaMR", "addArmaPorAta", "addArmaPorDef", "addArmaPA", $id, $conn);
	updateIntoArmas("listaArmasMele", "armaNombre", "armaDanyo", "armaMR", "armaPorAta", "armaPorDef", "armaPA", "armaId", $id, $conn);
	
	//armas escudo
	insertIntoArmas("listaArmasEscudos", "addEscudoNombre", "addEscudoDanyo", "addEscudoMR", "addEscudoPorAta", "addEscudoPorDef", "addEscudoPA", $id, $conn);
	updateIntoArmas("listaArmasEscudos", "escudoNombre", "escudoDanyo", "escudoMR", "escudoPorAta", "escudoPorDef", "escudoPA", "escudoId", $id, $conn);
	
	//armas distancia
	insertIntoArmas("listaArmasDistancia", "addArmaDistanciaNombre", "addArmaDistanciaDanyo", "addArmaDistanciaTasaFuego", "addArmaDistanciaPorAta", "addArmaDistanciaPorDef", "addArmaDistanciaAlcance", $id, $conn);
	updateIntoArmas("listaArmasDistancia", "armaDistanciaNombre", "armaDistanciaDanyo", "armaDistanciaTasaFuego", "armaDistanciaPorAta", "armaDistanciaPorDef", "armaDistanciaAlcance", "armaDistanciaId", $id, $conn);

}
dbClose($conn);

//al terminar redirección a la hoja
header('Location: /hoja/?id='.$id);

?>