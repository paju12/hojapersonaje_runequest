<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="manipulacion-lab">Bonificador Manipulación</span></div>
                    <input type="number" class="form-control" id="manipulacion" aria-describedby="manipulacion-lab" name="habManBonificador" value="<?= $listaManipulacion['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="manipulacion-inventar-lab">Inventar (05)</span></div>
                    <input type="number" class="form-control" id="manipulacion-inventar" aria-describedby="manipulacion-inventar-lab" name="habManInv" value="<?= $listaManipulacion['inventar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="manipulacion-ocultar-lab">Ocultar (05)</span></div>
                    <input type="number" class="form-control" id="manipulacion-ocultar" aria-describedby="manipulacion-ocultar-lab" name="habManOcu" value="<?= $listaManipulacion['ocultar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="manipulacion-trucos-lab">Trucos de manos (05)</span></div>
                    <input type="number" class="form-control" id="manipulacion-trucos" aria-describedby="manipulacion-trucos-lab" name="habManTru" value="<?= $listaManipulacion['trucos']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="col-12"><hr></div>
        <div class="col-12 text-center"><span class="h4">Tocar Instrumentos (00)</span></div>
        <div class="row">
            <div class="col-12 mt-2">
                <div id="addManipulacion" class="row">
                    <?php foreach($listaExtraManipulacion as $extraManipulacion): ?>
                        <div class="col-12 col-md-6 d-inline">
                            <div class="row">
                                <div class="input-group mb-2 col">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <input type="text" name="manipulacionNombre[]" value="<?= $extraManipulacion['nombreHabilidad']; ?>" class="d-none nombre">
                                            <input type="text" name="manipulacionId[]" value="<?= $extraManipulacion['id']; ?>" class="d-none">
                                            <?= $extraManipulacion['nombreHabilidad']; ?>
                                        </span>
                                    </div>
                                    <input type="number" class="form-control" aria-describedby="manipulacion-trepar-lab" name="manipulacionValor[]" value="<?= $extraManipulacion['valorHabilidad']; ?>" max="999">
                                </div>
                                <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addManipulacion()"></i>
            </div>
        </div>
    </div>
</div>

<script>
    function addManipulacion() {
        var nombre = prompt("Nombre de la propiedad", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addManipulacion").append('<div class="col-12 col-md-6 d-inline"> <div class="row"> <div class="input-group mb-2 col"> <div class="input-group-prepend"><span class="input-group-text"><input type="text" name="addManipulacionNombre[]" value="'+nombre+'" class="d-none nombre">'+nombre+'</span></div> <input type="number" class="form-control" aria-describedby="manipulacion-extra-lab" name="addManipulacionValor[]" value="0"> </div> <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i> </div> </div>');
        scrollNuevoCampo();
    }
</script>