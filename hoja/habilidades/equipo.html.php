<div class="mt-3" id="addEquipo">
    <div class="row">
        <span class="col text-center">Objeto</span>
        <span class="col-2 text-center">CAR</span>
        <span class="col-1 text-center pl-0">Borrar</span>
    </div>
    <hr/>
    <?php foreach($listaExtraEquipo as $equipo): ?>
        <div class="row">
            <div class="col d-inline">
                <div class="row">
                    <div class="input-group col">
                        <input type="text" name="equipoNombre[]" value="<?= $equipo['equipo']; ?>" class="form-control">
                        <input type="number" name="equipoCar[]" max="999" step="any" value="<?= $equipo['car']; ?>" class="form-control col-2 text-center carEquipo" onchange="recalcularCarEquipo();">
                        <input type="number" name="equipoId[]" value="<?= $equipo['id']; ?>" class="d-none">
                        <div class="btn col-1 text-center" onclick="borrarEquipo(this)"><i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1"></i></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="row">
    <div class="col d-inline">
        <div class="row">
            <div class="input-group col">
                <input type="text" value=" -- CAR TOTAL -- " class="form-control text-center" disabled>
                <input type="number" class="form-control col-2 text-center" id="carTotalEquipo" disabled>
                <div class="btn col-1 text-center" onclick="recalcularCarEquipo()"><i class="fas fa-sync-alt verde pl-0 pt-2 cursorPointer col-1" id="icoRecalcularCarEquipo"></i></div>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-center mt-2">
    <div class="col-1">
        <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addEquipamiento()"></i>
    </div>
</div>

<script>
    function borrarEquipo(obj) {
        if (!confirm("¿Desea borrar el equipo "+$(obj).parent().children("input").first().val()+"?")) { return; }
        $(obj).parent().parent().parent().parent().addClass("d-none");
        $(obj).parent().children("input").first().val("-99999");
        $(obj).parent().children(".carEquipo").first().val("0");
        recalcularCarEquipo();
    };

    function borrarEquipoNoEnBD(obj) {
        if (!confirm("¿Desea borrar el equipo "+$(obj).parent().children("input").first().val()+"?")) { return; }
        $(obj).parent().parent().parent().parent().remove();
        recalcularCarEquipo();
    };

    function addEquipamiento() {
        $("#addEquipo").append('<div class="row"><div class="col d-inline"><div class="row"><div class="input-group col"><input type="text" name="addEquipoNombre[]" class="form-control"><input type="number" name="addEquipoCar[]" max="999" step="any" class="form-control col-2 text-center carEquipo" onchange="recalcularCarEquipo();" value="0"><div class="btn col-1 text-center" onclick="borrarEquipoNoEnBD(this)"><i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1"></i></div></div></div></div></div>');
        scrollNuevoCampo();
    }
</script>