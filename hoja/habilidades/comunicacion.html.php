<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><label class="input-group-text" id="comunicacion-lab" for="comunicacion">Bonificador Comunicación</label></div>
                    <input type="number" class="form-control" id="comunicacion" aria-describedby="comunicacion-lab" name="habComBonificador" value="<?= $listaComunicacion['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row" id="addComunicacion">
            <div class="col-12 col-lg-4">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><label class="input-group-text" id="comunicacion-cantar-lab" for="comunicacion-cantar">Cantar (05)</label></div>
                    <input type="number" class="form-control" id="comunicacion-cantar" aria-describedby="comunicacion-cantar-lab" name="habComCantar" value="<?= $listaComunicacion['cantar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><label class="input-group-text" id="comunicacion-hablaFluida-lab" for="comunicacion-hablaFluida">Habla Fluida (05)</label></div>
                    <input type="number" class="form-control" id="comunicacion-hablaFluida" aria-describedby="comunicacion-hablaFluida-lab" name="habComHablaFluida" value="<?= $listaComunicacion['hablaFluida']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><label class="input-group-text" id="comunicacion-oratoria-lab" for="comunicacion-oratoria">Oratoria (05)</label></div>
                    <input type="number" class="form-control" id="comunicacion-oratoria" aria-describedby="comunicacion-oratoria-lab" name="habComOratoria" value="<?= $listaComunicacion['oratoria']; ?>" max="999">
                </div>
            </div>
            <div class="col-12"><hr></div>
            <div class="col-12 text-center"><span class="h4">Idiomas</span></div>
            <div class="col-12 mt-2">
                <div id="addComunicacionIdiomas" class="row">
                    <?php foreach($listaExtraComunicacionIdiomas as $idioma): ?>
                        <div class="col-12 col-md-6 d-inline">
                            <div class="row">
                                <div class="input-group mb-2 col">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><input type="text" name="comunicacionIdiomasNombre[]" value="<?= $idioma['nombreHabilidad']; ?>" class="d-none nombre"><?= $idioma['nombreHabilidad']; ?></span>
                                    </div>
                                    <input type="number" class="form-control" aria-describedby="comunicacionIdiomas-extra-lab" name="comunicacionIdiomasValor[]" value="<?= $idioma['valorHabilidad']; ?>" max="999"> 
                                    <input type="number" class="form-control d-none" name="comunicacionIdiomasId[]" value="<?= $idioma['id']; ?>"> 
                                </div> 
                                <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i> 
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addComunicacionIdioma()"></i>
            </div>
        </div>
    </div>
</div>

<script>
    function addComunicacionIdioma() {
        var nombre = prompt("Idioma", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addComunicacionIdiomas").append('<div class="col-12 col-md-6 d-inline"><div class="row"><div class="input-group mb-2 col"><div class="input-group-prepend"><span class="input-group-text"><input type="text" name="addComunicacionIdiomasNombre[]" value="'+nombre+'" class="d-none nombre">'+nombre+'</span></div><input type="number" class="form-control" aria-describedby="comunicacionIdiomas-extra-lab" name="addComunicacionIdiomasValor[]" value="0"></div><i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i></div></div>');
        scrollNuevoCampo();
    }
</script>