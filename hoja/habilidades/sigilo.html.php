<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-lab">Bonificador Sigilo</span></div>
                    <input type="number" class="form-control" id="percepcion" aria-describedby="percepcion-lab" name="habSigBonificador" value="<?= $listaSigilo['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-deslizarse-lab">Deslizarse en silencio (10)</span></div>
                    <input type="number" class="form-control" id="percepcion-deslizarse" aria-describedby="percepcion-deslizarse-lab" name="habSigDes" value="<?= $listaSigilo['deslizarse']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-esconderse-lab">Esconderse (10)</span></div>
                    <input type="number" class="form-control" id="percepcion-esconderse" aria-describedby="percepcion-esconderse-lab" name="habSigEsc" value="<?= $listaSigilo['esconderse']; ?>" max="999">
                </div>
            </div>
        </div>
    </div>
</div>
