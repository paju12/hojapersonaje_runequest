<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-lab">Bonificador Agilidad</span></div>
                    <input type="number" class="form-control" id="agilidad" aria-describedby="agilidad-lab" name="habAgiBonificador" value="<?= $listaAgilidad['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row" id="addAgilidad">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-arrojar-lab">Arrojar (25)</span></div>
                    <input type="number" class="form-control" id="agilidad-arrojar" aria-describedby="agilidad-arrojar-lab" name="habAgiArrojar" value="<?= $listaAgilidad['arrojar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-esquivar-lab">Esquivar (05)</span></div>
                    <input type="number" class="form-control" id="agilidad-esquivar" aria-describedby="agilidad-esquivar-lab" name="habAgiEsquivar" value="<?= $listaAgilidad['esquivar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-montar-lab">Montar (05)</span></div>
                    <input type="number" class="form-control" id="agilidad-montar" aria-describedby="agilidad-montar-lab" name="habAgiMontar" value="<?= $listaAgilidad['montar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-nadar-lab">Nadar (15)</span></div>
                    <input type="number" class="form-control" id="agilidad-nadar" aria-describedby="agilidad-nadar-lab" name="habAgiNadar" value="<?= $listaAgilidad['nadar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-1">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-remar-lab">Remar (05)</span></div>
                    <input type="number" class="form-control" id="agilidad-remar" aria-describedby="agilidad-remar-lab" name="habAgiRemar" value="<?= $listaAgilidad['remar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-saltar-lab">Saltar (25)</span></div>
                    <input type="number" class="form-control" id="agilidad-saltar" aria-describedby="agilidad-saltar-lab" name="habAgiSaltar" value="<?= $listaAgilidad['saltar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="agilidad-trepar-lab">Trepar (40)</span></div>
                    <input type="number" class="form-control" id="agilidad-trepar" aria-describedby="agilidad-trepar-lab" name="habAgiTrepar" value="<?= $listaAgilidad['trepar']; ?>" max="999">
                </div>
            </div>
            <?php foreach($listaExtraAgilidad as $extraAgilidad): ?>
                <div class="col-12 col-md-6 d-inline">
                    <div class="row">
                        <div class="input-group mb-2 col">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <input type="text" name="agilidadNombre[]" value="<?= $extraAgilidad['nombreHabilidad']; ?>" class="d-none nombre">
                                    <input type="text" name="agilidadId[]" value="<?= $extraAgilidad['id']; ?>" class="d-none">
                                    <?= $extraAgilidad['nombreHabilidad']; ?>
                                </span>
                            </div>
                            <input type="number" class="form-control" aria-describedby="agilidad-trepar-lab" name="agilidadValor[]" value="<?= $extraAgilidad['valorHabilidad']; ?>" max="999">
                        </div>
                        <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addAgilidad()"></i>
            </div>
        </div>
    </div>
</div>

<script>
    function addAgilidad() {
        var nombre = prompt("Nombre de la propiedad", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addAgilidad").append('<div class="col-12 col-md-6 d-inline"> <div class="row"> <div class="input-group mb-2 col"> <div class="input-group-prepend"><span class="input-group-text"><input type="text" name="addAgilidadNombre[]" value="'+nombre+'" class="d-none nombre">'+nombre+'</span></div> <input type="number" class="form-control" aria-describedby="agilidad-extra-lab" name="addAgilidadValor[]" value="0"> </div> <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i> </div> </div>');
        scrollNuevoCampo();
    }
</script>