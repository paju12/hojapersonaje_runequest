<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-lab">Bonificador Percepción</span></div>
                    <input type="number" class="form-control" id="percepcion" aria-describedby="percepcion-lab" name="habPerBonificador" value="<?= $listaPercepcion['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-buscar-lab">Buscar (25)</span></div>
                    <input type="number" class="form-control" id="percepcion-buscar" aria-describedby="percepcion-buscar-lab" name="habPerBus" value="<?= $listaPercepcion['buscar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-escuchar-lab">Escuchar (25)</span></div>
                    <input type="number" class="form-control" id="percepcion-escuchar" aria-describedby="percepcion-escuchar-lab" name="habPerEsc" value="<?= $listaPercepcion['escuchar']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-otear-lab">Otear (25)</span></div>
                    <input type="number" class="form-control" id="percepcion-otear" aria-describedby="percepcion-otear-lab" name="habPerOte" value="<?= $listaPercepcion['otear']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="percepcion-rastrear-lab">Rastrear (05)</span></div>
                    <input type="number" class="form-control" id="percepcion-rastrear" aria-describedby="percepcion-rastrear-lab" name="habPerRas" value="<?= $listaPercepcion['rastrear']; ?>" max="999">
                </div>
            </div>
        </div>
    </div>
</div>
