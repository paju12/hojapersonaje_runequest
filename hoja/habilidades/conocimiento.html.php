<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-lab">Bonificador Conocimiento</span></div>
                    <input type="number" class="form-control" id="conocimiento" aria-describedby="conocimiento-lab" name="habConBonificador" value="<?= $listaConocimiento['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-artes-marciales-lab">Artes marciales (00)</span></div>
                    <input type="number" class="form-control" id="conocimiento-marciales" aria-describedby="conocimiento-artes-marciales-lab" name="habConMar" value="<?= $listaConocimiento['marciales']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-animal-lab">Animal (05)</span></div>
                    <input type="number" class="form-control" id="conocimiento-animal" aria-describedby="conocimiento-animal-lab" name="habConAnimal" value="<?= $listaConocimiento['animal']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-mundo-lab">Mundo (05)</span></div>
                    <input type="number" class="form-control" id="conocimiento-mundo" aria-describedby="conocimiento-mundo-lab" name="habConMundo" value="<?= $listaConocimiento['mundo']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-humano-lab">Humano (15)</span></div>
                    <input type="number" class="form-control" id="conocimiento-humano" aria-describedby="conocimiento-humano-lab" name="habConHumano" value="<?= $listaConocimiento['humano']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-1">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-mineral-lab">Mineral (05)</span></div>
                    <input type="number" class="form-control" id="conocimiento-mineral" aria-describedby="conocimiento-mineral-lab" name="habConMineral" value="<?= $listaConocimiento['mineral']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-vegetal-lab">Vegetal (05)</span></div>
                    <input type="number" class="form-control" id="conocimiento-vegetal" aria-describedby="conocimiento-vegetal-lab" name="habConVegetal" value="<?= $listaConocimiento['vegetal']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-navegacion-lab">Navegación (00)</span></div>
                    <input type="number" class="form-control" id="conocimiento-navegacion" aria-describedby="conocimiento-navegacion-lab" name="habConNavegacion" value="<?= $listaConocimiento['navegacion']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-1auxilios-lab">1 Auxilios (10)</span></div>
                    <input type="number" class="form-control" id="conocimiento-1auxilios" aria-describedby="conocimiento-1auxilios-lab" name="habCon1Auxilios" value="<?= $listaConocimiento['1auxilios']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="conocimiento-valorarObjetos-lab">Valorar objetos (25)</span></div>
                    <input type="number" class="form-control" id="conocimiento-valorarObjetos" aria-describedby="conocimiento-valorarObjetos-lab" name="habConValorarObjetos" value="<?= $listaConocimiento['valorarObjetos']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="col-12"><hr></div>
        <div class="col-12 text-center"><span class="h4">Fabricación (10)</span></div>
        <div class="row">
            <div class="col-12 mt-2">
                <div id="addComunicacionFabricacion" class="row">
                    <?php foreach($listaExtraConocimientoFabricacion as $extraConocimiento): ?>
                        <div class="col-12 col-md-6 d-inline">
                            <div class="row">
                                <div class="input-group mb-2 col">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <input type="text" name="conocimientoFabricacionNombre[]" value="<?= $extraConocimiento['nombreHabilidad']; ?>" class="d-none nombre">
                                            <input type="text" name="conocimientoFabricacionId[]" value="<?= $extraConocimiento['id']; ?>" class="d-none">
                                            <?= $extraConocimiento['nombreHabilidad']; ?>
                                        </span>
                                    </div>
                                    <input type="number" class="form-control" aria-describedby="conocimiento-trepar-lab" name="conocimientoFabricacionValor[]" value="<?= $extraConocimiento['valorHabilidad']; ?>" max="999">
                                </div>
                                <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addConocimientoFab()"></i>
            </div>
        </div>

        <div class="col-12"><hr></div>
        <div class="col-12 text-center"><span class="h4">Leer/Escribir Idiomas</span></div>
        <div class="row">
            <div class="col-12 mt-2">
                <div id="addComunicacionLeer" class="row">
                    <?php foreach($listaExtraConocimientoLeer as $extraConocimiento): ?>
                        <div class="col-12 col-md-6 d-inline">
                            <div class="row">
                                <div class="input-group mb-2 col">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <input type="text" name="conocimientoLeerNombre[]" value="<?= $extraConocimiento['nombreHabilidad']; ?>" class="d-none nombre">
                                            <input type="text" name="conocimientoLeerId[]" value="<?= $extraConocimiento['id']; ?>" class="d-none">
                                            <?= $extraConocimiento['nombreHabilidad']; ?>
                                        </span>
                                    </div>
                                    <input type="number" class="form-control" aria-describedby="conocimiento-trepar-lab" name="conocimientoLeerValor[]" value="<?= $extraConocimiento['valorHabilidad']; ?>" max="999">
                                </div>
                                <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addConocimientoIdi()"></i>
            </div>
        </div>
    </div>
</div>

<script>
    function addConocimientoFab() {
        var nombre = prompt("Nombre de la propiedad", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addComunicacionFabricacion").append('<div class="col-12 col-md-6 d-inline"> <div class="row"> <div class="input-group mb-2 col"> <div class="input-group-prepend"><span class="input-group-text"><input type="text" name="addConocimientoFabNombre[]" value="'+nombre+'" class="d-none nombre">'+nombre+'</span></div> <input type="number" class="form-control" aria-describedby="conocimiento-extra-lab" name="addConocimientoFabValor[]" value="0"> </div> <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i> </div> </div>');
        scrollNuevoCampo();
    }

    function addConocimientoIdi() {
        var nombre = prompt("Nombre de la propiedad", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addComunicacionLeer").append('<div class="col-12 col-md-6 d-inline"> <div class="row"> <div class="input-group mb-2 col"> <div class="input-group-prepend"><span class="input-group-text"><input type="text" name="addConocimientoIdiNombre[]" value="'+nombre+'" class="d-none nombre">'+nombre+'</span></div> <input type="number" class="form-control" aria-describedby="conocimiento-extra-lab" name="addConocimientoIdiValor[]" value="0"> </div> <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i> </div> </div>');
        scrollNuevoCampo();
    }
</script>