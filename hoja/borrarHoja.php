<?php
require('../base/functions.php');
require('../base/checkLogin.php');
if(empty($_GET['id'])) { return "400 - Error al borrar"; } 

$conn = dbCon();

$stmt = prepared_query($conn, "SELECT propietario FROM hojas WHERE id = ? ORDER BY id DESC", [$_GET['id']]);
$hoja = $stmt->get_result()->fetch_assoc();
$stmt->close();

if ($hoja['propietario'] != getUsuFromCoockieSesion()) { return "400 - Error al borrar"; } 

prepared_query($conn, "DELETE FROM hojas WHERE id=?", [$_GET['id']], "i")->close();
dbClose($conn);

echo "200";