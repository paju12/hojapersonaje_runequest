<?php 
/* session_start();
if(empty($_SESSION['user'])) {
    return header('Location: /login/?urlFrom=/hoja/?id='.$_GET['id']); 
} */
require('../base/functions.php');
require('../base/checkLogin.php'); 

if (!empty($_GET['id'])) {
    $idHoja = $_GET['id'];

    $conn = dbCon();
    $stmt = prepared_query($conn, "SELECT * FROM hojas WHERE id = ? ORDER BY id DESC", [$idHoja]);
    $hoja = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    if (getUsuFromCoockieSesion() != false && $hoja['propietario'] != getUsuFromCoockieSesion()) {
        return header('Location: /lista?err=No tienes permiso para visualizar la hoja con id '.$idHoja);
    }

    function getListaDB($tabla, $idHoja, $conn) {
        $stmt = prepared_query($conn, "SELECT * FROM ".$tabla." WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
        $lista = $stmt->get_result();
        $stmt->close();
        return $lista;
    }
    
	function getTablaDB($tabla, $idHoja, $conn) {
        $stmt = prepared_query($conn, "SELECT * FROM ".$tabla." WHERE hojaRelacionada = ?", [$idHoja]);
		$arr = $stmt->get_result()->fetch_assoc();
		$stmt->close();
        return $arr;
    }

    //lista de equipos
    $stmt = prepared_query($conn, "SELECT * FROM listaEquipo WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraEquipo = $stmt->get_result();
    $stmt->close();

    //habilidades de agilidad
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesAgilidad WHERE hojaRelacionada = ?", [$idHoja]);
    $listaAgilidad = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    
    $stmt = prepared_query($conn, "SELECT * FROM listaExtraAgilidad WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraAgilidad = $stmt->get_result();
    $stmt->close();

    //habilidades de comunicación
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesComunicacion WHERE hojaRelacionada = ?", [$idHoja]);
    $listaComunicacion = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    $stmt = prepared_query($conn, "SELECT * FROM listaExtraComunicacion WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraComunicacionIdiomas = $stmt->get_result();
    $stmt->close();

    //habilidades de conocimiento
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesConocimiento WHERE hojaRelacionada = ?", [$idHoja]);
    $listaConocimiento = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    $stmt = prepared_query($conn, "SELECT * FROM listaExtraConocimientoFabricacion WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraConocimientoFabricacion = $stmt->get_result();
    $stmt->close();

    $stmt = prepared_query($conn, "SELECT * FROM listaExtraConocimientoLeer WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraConocimientoLeer = $stmt->get_result();
    $stmt->close();

    //Habilidades manipulacion
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesManipulacion WHERE hojaRelacionada = ?", [$idHoja]);
    $listaManipulacion = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    
    $stmt = prepared_query($conn, "SELECT * FROM listaExtraManipulacion WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraManipulacion = $stmt->get_result();
    $stmt->close();
    
    //habilidades percepcion
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesPercepcion WHERE hojaRelacionada = ?", [$idHoja]);
    $listaPercepcion = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    
    //habilidades sigilo
    $stmt = prepared_query($conn, "SELECT * FROM habilidadesSigilo WHERE hojaRelacionada = ?", [$idHoja]);
    $listaSigilo = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    //Habilidades con armas
	$armas = getTablaDB("armas", $idHoja, $conn);
    $listaArmasMele = getListaDB("listaArmasMele", $idHoja, $conn);
    $listaArmasDistancia = getListaDB("listaArmasDistancia", $idHoja, $conn);
    $listaArmasEscudos = getListaDB("listaArmasEscudos", $idHoja, $conn);

    //Magia
    $stmt = prepared_query($conn, "SELECT * FROM magia WHERE hojaRelacionada = ?", [$idHoja]);
    $listaMagia = $stmt->get_result()->fetch_assoc();
    $stmt->close();

    $stmt = prepared_query($conn, "SELECT * FROM listaExtraMagia WHERE hojaRelacionada = ? ORDER BY id ASC", [$idHoja]);
    $listaExtraMagia = $stmt->get_result();
    $stmt->close();

    dbClose($conn);
}

require('index.html.php');

?>