<?php 
// echo "<pre>"; var_dump($hoja); echo "</pre>";
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php require('../base/head.html'); ?>

	<title>RuneQuest - hoja de personaje</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<span class="navbar-brand">Hojas de Rol</span>

		<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
			<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item">
					<a class="nav-link" href="/lista" onclick="borrarCookieVista();">Lista de hojas</a>
				</li>
			</ul>
			<span class="navbar-brand"><?php echo getUsuFromCoockieSesion();?></span>
			<a href="/logout.php"><button class="btn btn-outline-danger my-2 my-sm-0" type="submit" onclick="borrarCookieVista();">Log out</button></a>
		</div>
	</nav>

	<div class="container mb-5 mt-2">
		<form action="guardarHoja.php" method="post">
			<input type="number" value="<?php echo toString($hoja['id']);?>" class="d-none" name="id">

			<div class="row justify-content-end mb-2 mr-2">
				<button class="btn btn-outline-success" id="btn-guardar"  name="submit" value="submit" type="submit" onclick="comprobarForm();"><i class="fas fa-save" id="ico-btn-guardar"></i></button>
			</div>

			<div class="row">
				<div class="col">
					<div class="card">
						<div class="card-body">
							<!-- Información personal -->
							<div class="row mb-2">
								<div class="col"><a class="" data-toggle="collapse" href="#collapseInfoPersonal" role="button" aria-expanded="true" aria-controls="collapseInfoPersonal" id="aInformacionPersonal"><h4>Información personal <i class="fas fa-plus fa-xs pl-2" id="iconoInformacionPersonal"></i></h4></a></div>
							</div>
							<div class="collapse <?php if(empty($hoja['nombreAventurero'])){echo 'show';}?>" id="collapseInfoPersonal">
								<div class="row">
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="nombreAventurero-lab">Nombre del aventurero</span></div>
											<input type="text" class="form-control" id="nombreAventurero" aria-describedby="nombreAventurero-lab" required name="nombreAventurero" value="<?php echo toString($hoja['nombreAventurero']);?>">
										</div>
									</div>
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="nombreJugador-lab">Nombre del jugador</span></div>
											<input type="text" class="form-control" id="nombreJugador" aria-describedby="nombreJugador-lab" name="nombreJugador" value="<?php echo toString($hoja['nombreJugador']);?>">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="especie-lab">Especie</span></div>
											<input type="text" class="form-control" id="especie" aria-describedby="especie-lab" name="especie" value="<?php echo toString($hoja['especie']);?>">
										</div>
									</div>
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="clan-lab">Clan de procedencia</span></div>
											<input type="text" class="form-control" id="clan" aria-describedby="clan-lab" name="clan" value="<?php echo toString($hoja['clanProcedencia']);?>">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-lg">
										<div class="row">
											<div class="col align-self-center">
												<div class="input-group mb-2">
													<div class="input-group-prepend"><span class="input-group-text" id="edad-lab">Edad</span></div>
													<input type="number" class="form-control" id="edad" aria-describedby="edad-lab" name="edad" value="<?php echo toString($hoja['edad']);?>">
												</div>
											</div>
											<div class="col-sm-12 col-md">
												<div class="input-group mb-2">
													<div class="input-group-prepend"><span class="input-group-text" id="genero-lab">Genero</span></div>
													<div class="card col">
														<div class="card-body">
															<div class="row">
																<div class="col align-self-center">
																	<div class="form-check">
																		<input class="form-check-input" type="radio" value="M" id="generoM" name="genero" <?php if(empty($_GET['id']) || $hoja['genero']=="M") {echo "checked";} ?> checked>
																		<label class="form-check-label" for="generoM">
																			M
																		</label>
																	</div>
																</div>
																<div class="col">
																	<div class="form-check">
																		<input class="form-check-input" type="radio" value="F" id="generoF" name="genero" <?php if($hoja['genero']=="F") {echo "checked";} ?>>
																		<label class="form-check-label" for="generoF">
																			F
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-12 col-lg align-self-center">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="profesionPadres-lab">Profesión de los padres</span></div>
											<input type="text" class="form-control" id="profesionPadres" aria-describedby="profesionPadres-lab" name="profesionPadres" value="<?php echo toString($hoja['profesionPadres']);?>">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="cultura-lab">Cultura</span></div>
											<input type="text" class="form-control" id="cultura" aria-describedby="cultura-lab" name="cultura" value="<?php echo toString($hoja['cultura']);?>">
										</div>
									</div>
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="profesionAventurero-lab">Profesión del Aventurero</span></div>
											<input type="text" class="form-control" id="profesionAventurero" aria-describedby="profesionAventurero-lab" name="profesionAventurero" value="<?php echo toString($hoja['profesionAventurero']);?>">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 col-lg">
										<div class="input-group mb-2">
											<div class="input-group-prepend"><span class="input-group-text" id="religion-lab">Religión</span></div>
											<input type="text" class="form-control" id="religion" aria-describedby="religion-lab" name="religion" value="<?php echo toString($hoja['religion']);?>">
										</div>
									</div>
								</div>
							</div>
							<!-- Características -->
							<div class="row mb-2">
								<div class="col"><h4>Características</h4></div>
							</div>
							<div class="row">
								<div class="col-12 col-lg pt-2">Actuales</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="fue-lab">FUE</span></div>
										<input type="number" class="form-control text-center" id="fue" aria-describedby="fue-lab" required name="fueAct" value="<?php echo toString($hoja['carAct_fue']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="con-lab">CON</span></div>
										<input type="number" class="form-control text-center" id="con" aria-describedby="con-lab" required name="conAct" value="<?php echo toString($hoja['carAct_con']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="tam-lab">TAM</span></div>
										<input type="number" class="form-control text-center" id="tam" aria-describedby="tam-lab" required name="tamAct" value="<?php echo toString($hoja['carAct_tam']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="int-lab">INT</span></div>
										<input type="number" class="form-control text-center" id="int" aria-describedby="int-lab" required name="iniAct" value="<?php echo toString($hoja['carAct_int']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="per-lab">PER</span></div>
										<input type="number" class="form-control text-center" id="per" aria-describedby="per-lab" required name="perAct" value="<?php echo toString($hoja['carAct_per']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="des-lab">DES</span></div>
										<input type="number" class="form-control text-center" id="des" aria-describedby="des-lab" required name="desAct" value="<?php echo toString($hoja['carAct_des']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-lab">ASP</span></div>
										<input type="number" class="form-control text-center" id="asp" aria-describedby="asp-lab" required name="aspAct" value="<?php echo toString($hoja['carAct_asp']);?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12 col-lg pt-2">Originales</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="fue-ori-lab">FUE</span></div>
										<input type="number" class="form-control text-center" id="fue-ori" aria-describedby="fue-ori-lab" name="fueOri" value="<?php echo toString($hoja['carOri_fue']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="con-ori-lab">CON</span></div>
										<input type="number" class="form-control text-center" id="con-ori" aria-describedby="con-ori-lab" name="conOri" value="<?php echo toString($hoja['carOri_con']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="tam-ori-lab">TAM</span></div>
										<input type="number" class="form-control text-center" id="tam-ori" aria-describedby="tam-ori-lab" name="tamOri" value="<?php echo toString($hoja['carOri_tam']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="int-ori-lab">INT</span></div>
										<input type="number" class="form-control text-center" id="int-ori" aria-describedby="int-ori-lab" name="intOri" value="<?php echo toString($hoja['carOri_int']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="per-ori-lab">PER</span></div>
										<input type="number" class="form-control text-center" id="per-ori" aria-describedby="per-ori-lab" name="perOri" value="<?php echo toString($hoja['carOri_per']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="des-ori-lab">DES</span></div>
										<input type="number" class="form-control text-center" id="des-ori" aria-describedby="des-ori-lab" name="desOri" value="<?php echo toString($hoja['carOri_des']);?>">
									</div>
								</div>
								<div class="col-4 col-lg p-1">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">ASP</span></div>
										<input type="number" class="form-control text-center" name="aspOri" value="<?php echo toString($hoja['carOri_asp']);?>">
									</div>
								</div>
							</div>
							<!-- Atributos seleccionados -->
							<div class="row mb-2">
								<div class="col"><h4>Atributos seleccionados</h4></div>
							</div>
							<div class="row">
								<div class="col-12 col-md-6 col-lg-3">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">Mod. Daño</span></div>
										<input type="text" class="form-control" required name="modDanyo" value="<?php echo toString0($hoja['atr_modDanyo']);?>">
									</div>
								</div>
								<div class="col-12 col-md-6 col-lg-2">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">Mov.</span></div>
										<input type="number" class="form-control" required name="mov" value="<?php echo toString0($hoja['atr_mov']);?>">
									</div>
								</div>
								<div class="col">
									<div class="input-group mb-2">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">MMR-DES</span></div>
										<input type="number" class="form-control" required name="mmrDes" id="mmrDes" value="<?php echo toString0($hoja['atr_mmr-des']);?>" onChange="sumarMmr();">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">+ MMR-TAM</span></div>
										<input type="number" class="form-control" required name="mmrTam" id="mmrTam" value="<?php echo toString0($hoja['atr_mmr-tam']);?>" onChange="sumarMmr();">
										<div class="input-group-prepend"><span class="input-group-text" id="asp-ori-lab">= MMR-CC</span></div>
										<input type="number" class="form-control" required name="mmrCc" id="mmrCc" value="<?php echo toString0($hoja['atr_mmr-cc']);?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-habilidades-tab" data-toggle="tab" href="#nav-habilidades" role="tab" aria-controls="nav-home" aria-selected="true" onclick="actualizarCookieVista('nav-habilidades-tab');">Habilidades</a>
							<a class="nav-item nav-link" id="nav-magia-tab" data-toggle="tab" href="#nav-magia" role="tab" aria-controls="nav-magia" aria-selected="false" onclick="actualizarCookieVista('nav-magia-tab');">Magia</a>
							<a class="nav-item nav-link" id="nav-armas-tab" data-toggle="tab" href="#nav-armas" role="tab" aria-controls="nav-armas" aria-selected="false" onclick="actualizarCookieVista('nav-armas-tab');">Habilidades con Armas</a>
							<a class="nav-item nav-link" id="nav-equipo-tab" data-toggle="tab" href="#nav-equipo" role="tab" aria-controls="nav-equipo" aria-selected="false" onclick="actualizarCookieVista('nav-equipo-tab');">Equipo</a>
							<a class="nav-item nav-link" id="nav-tablas-tab" data-toggle="tab" href="#nav-tablas" role="tab" aria-controls="nav-tablas" aria-selected="false" onclick="actualizarCookieVista('nav-tablas-tab');">Tablas de atributos</a>
							<a class="nav-item nav-link" id="nav-textoLibre-tab" data-toggle="tab" href="#nav-textoLibre" role="tab" aria-controls="nav-tablas" aria-selected="false" onclick="actualizarCookieVista('nav-textoLibre-tab');">Texto libre</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-habilidades" role="tabpanel" aria-labelledby="nav-habilidades-tab">
							<div class="row">
								<div class="nav flex-column nav-pills col-12 col-md-2" id="v-pills-tab" role="tablist" aria-orientation="vertical">
									<a class="nav-link active" id="v-pills-agilidad-tab" data-toggle="pill" href="#v-pills-agilidad" role="tab" aria-controls="v-pills-agilidad" aria-selected="true" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-agilidad-tab');">Agilidad</a>
									<a class="nav-link" id="v-pills-comunicacion-tab" data-toggle="pill" href="#v-pills-comunicacion" role="tab" aria-controls="v-pills-comunicacion" aria-selected="false" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-comunicacion-tab');">Comunicación</a>
									<a class="nav-link" id="v-pills-conocimiento-tab" data-toggle="pill" href="#v-pills-conocimiento" role="tab" aria-controls="v-pills-conocimiento" aria-selected="false" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-conocimiento-tab');">Conocimiento</a>
									<a class="nav-link" id="v-pills-manipulacion-tab" data-toggle="pill" href="#v-pills-manipulacion" role="tab" aria-controls="v-pills-manipulacion" aria-selected="false" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-manipulacion-tab');">Manipulación</a>
									<a class="nav-link" id="v-pills-percepcion-tab" data-toggle="pill" href="#v-pills-percepcion" role="tab" aria-controls="v-pills-percepcion" aria-selected="false" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-percepcion-tab');">Percepción</a>
									<a class="nav-link" id="v-pills-sigilo-tab" data-toggle="pill" href="#v-pills-sigilo" role="tab" aria-controls="v-pills-sigilo" aria-selected="false" onclick="actualizarCookieVista('nav-habilidades-tab|v-pills-sigilo-tab');">Sigilo</a>
								</div>
								<div class="tab-content col" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-agilidad" role="tabpanel" aria-labelledby="v-agilidad-home-tab">
										<?php require('./habilidades/agilidad.html.php'); ?>
									</div>
									<div class="tab-pane fade" id="v-pills-comunicacion" role="tabpanel" aria-labelledby="v-pills-comunicacion-tab">
										<?php require('./habilidades/comunicacion.html.php'); ?>
									</div>
									<div class="tab-pane fade" id="v-pills-conocimiento" role="tabpanel" aria-labelledby="v-pills-conocimiento-tab">
										<?php require('./habilidades/conocimiento.html.php'); ?>
									</div>
									<div class="tab-pane fade" id="v-pills-manipulacion" role="tabpanel" aria-labelledby="v-pills-manipulacion-tab">
										<?php require('./habilidades/manipulacion.html.php'); ?>
									</div>
									<div class="tab-pane fade" id="v-pills-percepcion" role="tabpanel" aria-labelledby="v-pills-percepcion-tab">
										<?php require('./habilidades/percepcion.html.php'); ?>
									</div>
									<div class="tab-pane fade" id="v-pills-sigilo" role="tabpanel" aria-labelledby="v-pills-sigilo-tab">
										<?php require('./habilidades/sigilo.html.php'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-magia" role="tabpanel" aria-labelledby="nav-magia-tab">
							<?php require('./magia/magia.html.php'); ?>
						</div>
						<div class="tab-pane fade" id="nav-armas" role="tabpanel" aria-labelledby="nav-armas-tab">
							<?php require('./armas/armas.html.php'); ?>
						</div>
						<div class="tab-pane fade" id="nav-equipo" role="tabpanel" aria-labelledby="nav-equipo-tab">
							<?php require('./habilidades/equipo.html.php'); ?>
						</div>
						<div class="tab-pane fade" id="nav-tablas" role="tabpanel" aria-labelledby="nav-tablas-tab">...</div>
						<div class="tab-pane fade" id="nav-textoLibre" role="tabpanel" aria-labelledby="nav-textoLibre-tab">
							<textarea class="form-control mt-2" id="textoLibre" rows="15" name="textoLibre"><?php echo toString($hoja['textoLibre']);?></textarea>
						</div>
					</div>
				</div>
			</div>

		</form>
	</div>


	<script>
		$( document ).ready(function() {
			if (!(Cookies.get('posicionVista') === 'undefined' || Cookies.get('posicionVista') === '')) {
				var pos = Cookies.get('posicionVista').split("|");
				pos.forEach(function(v, i){
					$('#'+v).click();
				});
			}
			recalcularCarEquipo();
		});

		function actualizarCookieVista(pos) {
			Cookies.set('posicionVista', pos, {expires: 1});
		}
		function borrarCookieVista() { Cookies.remove('posicionVista'); }

		$("#aInformacionPersonal").on('click', function () {
			$("#iconoInformacionPersonal")
			.toggleClass('fa-plus')
			.toggleClass('fa-minus');
		});

		function borrar(obj) {
			var row = $(obj).parent().parent();
			if (!confirm("¿Desea borrar la propiedad "+row.find(".nombre").first().val()+"?")) { return; }
			row.removeClass('d-inline');
			row.addClass("d-none");
			$(obj).parent().children("div").first().children("input").first().val("-99999");
		};

		function borrarNoEnBD(obj) {
			if (!confirm("¿Desea borrar la propiedad "+$(obj).parent().parent().find(".nombre").first().val()+"?")) { return; }
			$(obj).parent().parent().remove();
		};

		function scrollNuevoCampo() {
			//scroll para que se vea el nuevo campo correctamente
			var y = $(window).scrollTop();  //your current y position on the page
			$(window).scrollTop(y+47);
		}
		
		function comprobarForm() {
			if (!$('#collapseInfoPersonal').hasClass('show') && jQuery.isEmptyObject($('#nombreAventurero').val())) {
				$('#collapseInfoPersonal').addClass('show');
			} else {
				$('#ico-btn-guardar').addClass('d-none');
				$('#btn-guardar').append('<lottie-player src="https://assets6.lottiefiles.com/packages/lf20_IelcBP.json"  background="transparent"  speed="1"  style="width: 25px; height: 25px;"  loop  autoplay></lottie-player>');
			}
		}

		function sumarMmr() {
			$('#mmrCc').val( (parseInt($('#mmrDes').val())||0) + (parseInt($('#mmrTam').val())||0) );
		}

		function recalcularCarEquipo() {
			$('#icoRecalcularCarEquipo').toggleClass('fa-spin');
			
			var total = 0;
			$('.carEquipo').get().forEach(function(v, i){
				var val = $(v).val();
				val = val || 0;//if NaN => 0
				total += parseFloat(val);
			});
			$('#carTotalEquipo').val(total);

			setTimeout(() => {  $('#icoRecalcularCarEquipo').toggleClass('fa-spin'); }, 100);
		}
	</script>

	<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
	<?php require('../base/scripts.html') ?>
</body>
</html>