<div class="card col">
    <div class="card-body">
        <div class="row mb-4">
            <div class="col">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-lab">Bonificador Magia</span></div>
                    <input type="number" class="form-control" id="magia" aria-describedby="magia-lab" name="magiaBonificador" value="<?= $listaMagia['bonificador']; ?>" max="999">
                </div>
            </div>
        </div>
        <div class="row" id="addMagia">
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-ceremonia-lab">Ceremonia (05)</span></div>
                    <input type="number" class="form-control" id="magia-ceremonia" aria-describedby="magia-ceremonia-lab" name="magiaCeremonia" value="<?= $listaMagia['ceremonia']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-encantamiento-lab">Encantamiento (00)</span></div>
                    <input type="number" class="form-control" id="magia-encantamiento" aria-describedby="magia-encantamiento-lab" name="magiaEncantamiento" value="<?= $listaMagia['encantamiento']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-invocacion-lab">Invocación (00)</span></div>
                    <input type="number" class="form-control" id="magia-invocacion" aria-describedby="magia-invocacion-lab" name="magiaInvocacion" value="<?= $listaMagia['invocacion']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-duracion-lab">Duración (00)</span></div>
                    <input type="number" class="form-control" id="magia-duracion" aria-describedby="magia-duracion-lab" name="magiaDuracion" value="<?= $listaMagia['duracion']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-1">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-intensidad-lab">Intensidad (00)</span></div>
                    <input type="number" class="form-control" id="magia-intensidad" aria-describedby="magia-intensidad-lab" name="magiaIntensidad" value="<?= $listaMagia['intensidad']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-multiconjuro-lab">Multiconjuro (00)</span></div>
                    <input type="number" class="form-control" id="magia-multiconjuro" aria-describedby="magia-multiconjuro-lab" name="magiaMulticonjuro" value="<?= $listaMagia['multiconjuro']; ?>" max="999">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-2">
                    <div class="input-group-prepend"><span class="input-group-text" id="magia-alcance-lab">Alcance (00)</span></div>
                    <input type="number" class="form-control" id="magia-alcance" aria-describedby="magia-alcance-lab" name="magiaAlcance" value="<?= $listaMagia['alcance']; ?>" max="999">
                </div>
            </div>
            <hr class="hr col-12">
            <div class="col-12 text-center mb-4">
                <span class="h4">Conjuros</span>
            </div>
            <?php foreach($listaExtraMagia as $extraMagia): ?>
                <div class="col-12 d-inline">
                    <div class="row">
                        <div class="input-group mb-2 col">
                            <div class="input-group-prepend p-0 col-9">
                                <input type="text" name="magiaNombre[]" value="<?= $extraMagia['nombreMagia']; ?>" class="nombre input-group-text col">
                                <input type="text" name="magiaId[]" value="<?= $extraMagia['id']; ?>" class="d-none">
                            </div>
                            <input type="number" class="form-control" aria-describedby="magia-alcance-lab" name="magiaValor[]" value="<?= $extraMagia['valorMagia']; ?>" max="999">
                        </div>
                        <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrar(this)"></i>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-1">
                <i class="fas fa-plus-circle verde fa-lg cursorPointer" onclick="addMagia()"></i>
            </div>
        </div>
    </div>
</div>

<script>
    function addMagia() {
        var nombre = prompt("Nombre de la propiedad", "");
        if (nombre == null || nombre == "") { return null; }
        $("#addMagia").append('<div class="col-12 d-inline"> <div class="row"> <div class="input-group mb-2 col"> <div class="input-group-prepend p-0 col-9"><input type="text" name="addMagiaNombre[]" value="'+nombre+'" class="nombre input-group-text col"></div> <input type="number" class="form-control" aria-describedby="magia-extra-lab" name="addMagiaValor[]" value="0" max="999"> </div> <i class="fas fa-trash-alt rojo pl-0 pt-2 cursorPointer col-1" onclick="borrarNoEnBD(this)"></i> </div> </div>');
        scrollNuevoCampo();
    }
</script>