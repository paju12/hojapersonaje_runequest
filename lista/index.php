<?php 
require('../base/functions.php');
require('../base/checkLogin.php');

$conn = dbCon();
$stmt = prepared_query($conn, "SELECT id, nombreAventurero FROM hojas WHERE propietario = ? ORDER BY id DESC", [getUsuFromCoockieSesion()]);
$hojas = $stmt->get_result();

require('index.html.php');

?>