<!DOCTYPE html>
<html lang="es">
<head>
    <?php require('../base/head.html'); ?>

    <title>Hojas de Rol - Lista de hojas de personajes</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <span class="navbar-brand active">Hojas de Rol</span>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="./">Lista de hojas</a>
                </li>
            </ul>
            <span class="navbar-brand"><?= getUsuFromCoockieSesion();?></span>
            <a href="/logout.php"><button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Log out</button></a>
        </div>
    </nav>

    <?php if(!empty($_GET['err'])) { ?>
        <button type="button" class="btn btn-primary d-none" data-toggle="modal" data-target="#errorModal" id="btnErrorModal" >error modal</button>
        <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-danger">Error</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><?php echo $_GET['err'];?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="container mb-5 mt-5">
            
        <div class="row mb-4">
            <div class="col text-center">
                <h2>Listado de hojas</h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col">
                <div class="list-group">
                    <?php foreach ($hojas as $hoja) { ?>
                        <div class="list-group-item list-group-item-action d-flex justify-content-between align-items-center p-0">
                            <a href="/hoja?id=<?php echo $hoja['id'];?>" class="col list-group-item-action p-3"><?php echo $hoja['nombreAventurero'];?></a>
                            <button class="btn col-1 list-group-item-action text-center col-3 col-md-2 col-lg-1" onclick="borrar(<?php echo $hoja['id'];?>, this)"><i class="fas fa-trash-alt text-danger p-3"></i></button>
                        </div>
                    <?php } ?>
                    <a href="/hoja" class="list-group-item list-group-item-action list-group-item-success text-center">Nueva hoja</a>
                </div>
            </div>
        </div>

    </div>

    <script>
       <?php if(!empty($_GET['err'])) { ?>
        $( document ).ready(function() {
            $('#btnErrorModal').click();
        });
        <?php } ?> 

        function borrar(id, obj) {
            if (confirm("Desea eliminar la hoja?") == true) {
                $(obj).notify("Eliminando", { position:"right" });
                $.ajax({
                    url: "/hoja/borrarHoja.php?id="+id,
                    method: "GET"
                })
                .done(function( data ) {
                    if (data == "200") {
                        $.notify("Eliminado correctamente", { position:"bottom right" })
                        $(obj).parent().remove();
                    } else {
                        alert(data);
                        //sino mostrar error(se puede aprobechar a redireccionar a esta misma pagina con el parametro del get err y mostrar el error)
                    }
                });
            }
        }
    </script>

    <?php require('../base/scripts.html') ?>
</body>
</html>