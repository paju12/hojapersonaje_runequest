<?php
require('../base/functions.php');

if (empty($_POST['user'])) {
    $error.="No se ha indicado el nombre de usuario. <br/>";
}
if (empty($_POST['pass'])) {
    $error.="No se ha indicado la contraseña. <br/>";
}
if ($_POST['pass'] != $_POST['passRepit']) {
    $error.="Las contraseñas no coinciden. <br/>";
}

if (!empty($error)) {
    header('Location: /login?err='.$error.'&user='.$_POST['user']);
}

$conn = dbCon();

$stmtUser = prepared_query($conn, "SELECT user FROM usuarios WHERE user = ?", [$_POST['user']]);
$user = $stmtUser->get_result()->fetch_assoc();

if (!empty($user['user'])) {
    cerrarDB($stmtUser, $conn);
    header('Location: /login?err=Ya existe un usuario con ese nombre, elija otro.&user='.$_POST['user']);
} else {
    prepared_query($conn, "INSERT INTO usuarios (user, password)VALUES(?,?) ", [$_POST['user'], hash("sha512", $_POST['pass'])])->close();
    cerrarDB($stmtUser, $conn);
    createCoockieSesion($_POST['user']);
    $urlFrom = $_POST['urlFrom'];
    if (empty($urlFrom) || $urlFrom == ""){
        header('Location: /');
    } else {
        header('Location: '.$urlFrom);
    }
}



function cerrarDB($stmt, $conn) {
    $stmt->close();
    dbClose($conn);
}