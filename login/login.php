<?php
require('../base/functions.php');

$url = "https://hcaptcha.com/siteverify";
$dataH = array('secret' => '0x7D4c9A440d66435f38dD27F2D65f313311e205CC', 'response' => $_POST['h-captcha-response']);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($dataH)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
$json = json_decode($result, true);

$urlFrom = $_POST['urlFrom'];
$urlFromStringError = "";
if (!(empty($urlFrom) || $urlFrom == "")){
    $urlFromStringError = "&urlFrom=".$urlFrom;
}

if ($result === false || empty($dataH) || $json["success"] === false) { 
    return header('Location: /login?err=No ha superado el captcha&user='.$_POST['user'].$urlFromStringError);
}

$conn = dbCon();
$stmt = prepared_query($conn, "SELECT user FROM usuarios WHERE user = ? AND password = ?", [$_POST['user'], hash("sha512", $_POST['pass'])]);
$user = $stmt->get_result()->fetch_assoc();

$nombre = $user['user'];

$stmt->close();
dbClose($conn);

if (empty($nombre)) {
    header('Location: /login?err=Usuario y/o contraseña incorrecto&user='.$_POST['user'].$urlFromStringError);
} else {
    createCoockieSesion($nombre);
    if (empty($urlFrom) || $urlFrom == ""){
        header('Location: /');
    } else {
        header('Location: '.$urlFrom);
    }
}



