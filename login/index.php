<?php 
    require('../base/functions.php');
    if(getUsuFromCoockieSesion() != false) { return header('Location: /'); } 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require('../base/head.html'); ?>

    <title>Login - Hojas de Rol</title>
</head>
<body class="bg-secondary">

    <div class="card col-sm-2" style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);">
        <div class="card-body m-4">
            <form action="login.php" method="post">
                <div class="row">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-user"></i></div>
                        </div>
                        <input class="form-control" type="text" placeholder="Usuario" name="user" id="user" value="<?php if(!empty($_GET['user'])){ echo $_GET['user'];} ?>" required autofocus>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-key"></i></div>
                        </div>
                        <input class="form-control" type="password" placeholder="Contraseña" name="pass" id="password" required>
                    </div>
                </div>
                <div class="row d-none" id="repetirContrasenyaRow">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-key"></i></div>
                        </div>
                        <input class="form-control" type="password" placeholder="Repetir Contraseña" name="passRepit" id="passwordRepit">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="h-captcha" data-sitekey="4c449972-de96-4f26-805e-38d360dc6e39" data-size="compact"></div>
                </div>
                <input type="text" name="urlFrom" value="<?= $_GET['urlFrom']; ?>" class="d-none">
                <div class="row justify-content-center mb-2">
                    <span class="text-muted cursorPointer" onclick="registratse();" id="cambiarAregistro">No tienes cuenta, pulsa aquí</span>
                </div>
                <div class="row justify-content-end">
                    <button class="btn btn-outline-success" type="submit" id="btnLogin">Login</button>
                </div>
            </form>
                <?php if(!empty($_GET['err'])) { ?>
                <div class="row justify-content-center" id="msgError">
                    <div class="card mt-2 bg-danger">
                        <div class="card-body text-center" onclick="$('#msgError').addClass('d-none');">
                            <span style="color: #FFF;"><?php echo $_GET['err']?></span>
                        </div>
                    </div>
                </div>
                <?php } ?>
        </div>
    </div>

    <script>
        function registratse() {
            if ($('#repetirContrasenyaRow').hasClass('d-none')){
                $('#cambiarAregistro').text('Volver al login');
                $("#btnLogin").text('Registrarse');
                $('form').attr('action', 'registro.php');
            } else {
                $('#cambiarAregistro').text('No tienes cuenta, pulsa aquí');
                $("#btnLogin").text('Login');
                $('form').attr('action', 'login.php');
            }
            $('#repetirContrasenyaRow').toggleClass('d-none');
        }
    </script>

    <?php require('../base/scripts.html') ?>
    <script src="https://hcaptcha.com/1/api.js" async defer></script>
</body>
</html>