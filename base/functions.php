<?php

function getIni() { return parse_ini_file('../config/config.ini', true); }
require('mcript.php');

function dbCon() {
    $ini = getIni();
    $con = mysqli_connect($ini['database']['db_host'], $ini['database']['db_user'], $ini['database']['db_password'], $ini['database']['db_name']);

    if (!$con) { die("Error al conectar con la base de datos");}

    return $con;
}

function dbClose($con) {
    mysqli_close($con);
}

function prepared_query($mysqli, $sql, $params, $types = "") {
    $types = $types ?: str_repeat("s", count($params));
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param($types, ...$params);
    $stmt->execute();
    return $stmt;
}

function toString($str) {
    if(empty($str)) { return ""; }
    return $str;
}

function toString0($str) {
    if(empty($str)) { return "0"; }
    return $str;
}

function createCoockieSesion(string $usu) {
    setcookie("sessionid", encriptar($usu), time()+43200, "/", "", true, true);
}

function deleteCoockieSesion() {
    setcookie("sessionid", "", time()-43200, "/", "", true, true);
}

function getUsuFromCoockieSesion() {
    if (empty($_COOKIE['sessionid'])) { return false; }
    return desencriptar($_COOKIE["sessionid"]);
}

?>