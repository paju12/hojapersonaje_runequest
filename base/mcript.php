<?php

// function getIni() { return parse_ini_file('../config/config.ini', true); }

$method = 'aes-256-cbc';

$encriptar = function ($valor) use ($method) {
    $ini = getIni();
    return openssl_encrypt ($valor, $method, $ini['encrypt']['key'], false, base64_decode($ini['encrypt']['iv']));
};

$desencriptar = function ($valor) use ($method) {
    $ini = getIni();
    // $encrypted_data = base64_decode($valor);
    return openssl_decrypt($valor, $method, $ini['encrypt']['key'], false, base64_decode($ini['encrypt']['iv']));
};

function encriptar($valor) {
    $ini = getIni();
    return openssl_encrypt ($valor, 'aes-256-cbc', $ini['encrypt']['key'], false, base64_decode($ini['encrypt']['iv']));
};

function desencriptar($valor) {
    $ini = getIni();
    return openssl_decrypt($valor, 'aes-256-cbc', $ini['encrypt']['key'], false, base64_decode($ini['encrypt']['iv']));
};

/*
Genera un valor para IV
*/
$getIV = function () use ($method) {
    return base64_encode(openssl_random_pseudo_bytes(openssl_cipher_iv_length($method)));
};