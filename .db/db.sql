SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `hojaRol`
--
CREATE DATABASE IF NOT EXISTS `hojaRol` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `hojaRol`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aguantePersonaje`
--

CREATE TABLE `aguantePersonaje` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `cabezaPA` int(11) NOT NULL,
  `cabezaPG` int(11) NOT NULL,
  `pechoPA` int(11) NOT NULL,
  `pechoPG` int(11) NOT NULL,
  `barzoDPA` int(11) NOT NULL,
  `brazoDPG` int(11) NOT NULL,
  `brazoIPA` int(11) NOT NULL,
  `brazoIPG` int(11) NOT NULL,
  `abdomenPA` int(11) NOT NULL,
  `abdomenPG` int(11) NOT NULL,
  `piernaDPA` int(11) NOT NULL,
  `piernaDPG` int(11) NOT NULL,
  `piernaIPA` int(11) NOT NULL,
  `piernaIPG` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesAgilidad`
--

CREATE TABLE `habilidadesAgilidad` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `arrojar` int(3) DEFAULT NULL,
  `esquivar` int(3) DEFAULT NULL,
  `montar` int(3) DEFAULT NULL,
  `nadar` int(3) DEFAULT NULL,
  `remar` int(3) DEFAULT NULL,
  `saltar` int(3) DEFAULT NULL,
  `trepar` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesComunicacion`
--

CREATE TABLE `habilidadesComunicacion` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `cantar` int(3) DEFAULT NULL,
  `hablaFluida` int(3) DEFAULT NULL,
  `oratoria` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesConocimiento`
--

CREATE TABLE `habilidadesConocimiento` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `marciales` int(3) DEFAULT NULL,
  `animal` int(3) DEFAULT NULL,
  `mundo` int(3) DEFAULT NULL,
  `humano` int(3) DEFAULT NULL,
  `mineral` int(3) DEFAULT NULL,
  `vegetal` int(3) DEFAULT NULL,
  `navegacion` int(3) DEFAULT NULL,
  `1auxilios` int(3) DEFAULT NULL,
  `valorarObjetos` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesManipulacion`
--

CREATE TABLE `habilidadesManipulacion` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `inventar` int(3) DEFAULT NULL,
  `ocultar` int(3) DEFAULT NULL,
  `trucos` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesPercepcion`
--

CREATE TABLE `habilidadesPercepcion` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `buscar` int(3) DEFAULT NULL,
  `escuchar` int(3) DEFAULT NULL,
  `otear` int(3) DEFAULT NULL,
  `rastrear` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidadesSigilo`
--

CREATE TABLE `habilidadesSigilo` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `deslizarse` int(3) DEFAULT NULL,
  `esconderse` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hojas`
--

CREATE TABLE `hojas` (
  `id` int(11) NOT NULL,
  `propietario` varchar(50) NOT NULL,
  `nombreAventurero` varchar(255) NOT NULL,
  `nombreJugador` varchar(255) DEFAULT NULL,
  `edad` int(5) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `especie` varchar(255) DEFAULT NULL,
  `cultura` varchar(255) DEFAULT NULL,
  `clanProcedencia` varchar(255) DEFAULT NULL,
  `profesionPadres` varchar(255) DEFAULT NULL,
  `profesionAventurero` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `carAct_fue` varchar(255) NOT NULL,
  `carAct_con` varchar(255) NOT NULL,
  `carAct_tam` varchar(255) NOT NULL,
  `carAct_int` varchar(255) NOT NULL,
  `carAct_per` varchar(255) NOT NULL,
  `carAct_des` varchar(255) NOT NULL,
  `carAct_asp` varchar(255) NOT NULL,
  `carOri_fue` varchar(255) DEFAULT NULL,
  `carOri_con` varchar(255) DEFAULT NULL,
  `carOri_tam` varchar(255) DEFAULT NULL,
  `carOri_int` varchar(255) DEFAULT NULL,
  `carOri_per` varchar(255) DEFAULT NULL,
  `carOri_des` varchar(255) DEFAULT NULL,
  `carOri_asp` varchar(255) DEFAULT NULL,
  `atr_modDanyo` varchar(255) NOT NULL,
  `atr_mov` int(11) NOT NULL,
  `atr_mmr-des` int(11) NOT NULL,
  `atr_mmr-tam` int(11) NOT NULL,
  `atr_mmr-cc` int(11) NOT NULL,
  `textoLibre` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaArmas`
--

CREATE TABLE `listaArmas` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `arma` varchar(255) NOT NULL,
  `danyo` varchar(255) NOT NULL,
  `movimientoReaccion` int(2) NOT NULL,
  `ataque` int(3) NOT NULL,
  `defensa` int(3) NOT NULL,
  `puntosAguante` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaEquipo`
--

CREATE TABLE `listaEquipo` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `equipo` varchar(255) NOT NULL,
  `car` float(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraAgilidad`
--

CREATE TABLE `listaExtraAgilidad` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraComunicacion`
--

CREATE TABLE `listaExtraComunicacion` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraConocimientoFabricacion`
--

CREATE TABLE `listaExtraConocimientoFabricacion` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraConocimientoLeer`
--

CREATE TABLE `listaExtraConocimientoLeer` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraMagia`
--

CREATE TABLE `listaExtraMagia` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreMagia` varchar(255) NOT NULL,
  `valorMagia` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraManipulacion`
--

CREATE TABLE `listaExtraManipulacion` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraPercepcion`
--

CREATE TABLE `listaExtraPercepcion` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `listaExtraSigilo`
--

CREATE TABLE `listaExtraSigilo` (
  `id` int(11) NOT NULL,
  `hojaRelacionada` int(11) NOT NULL,
  `nombreHabilidad` varchar(255) NOT NULL,
  `valorHabilidad` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `magia`
--

CREATE TABLE `magia` (
  `hojaRelacionada` int(11) NOT NULL,
  `bonificador` int(3) DEFAULT NULL,
  `ceremonia` int(3) DEFAULT NULL,
  `encantamiento` int(3) DEFAULT NULL,
  `invocacion` int(3) DEFAULT NULL,
  `duracion` int(3) DEFAULT NULL,
  `intensidad` int(3) DEFAULT NULL,
  `multiconjuro` int(3) DEFAULT NULL,
  `alcance` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aguantePersonaje`
--
ALTER TABLE `aguantePersonaje`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aguantePersonaje_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesAgilidad`
--
ALTER TABLE `habilidadesAgilidad`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesComunicacion`
--
ALTER TABLE `habilidadesComunicacion`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesConocimiento`
--
ALTER TABLE `habilidadesConocimiento`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesManipulacion`
--
ALTER TABLE `habilidadesManipulacion`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesPercepcion`
--
ALTER TABLE `habilidadesPercepcion`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `habilidadesSigilo`
--
ALTER TABLE `habilidadesSigilo`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- Indices de la tabla `hojas`
--
ALTER TABLE `hojas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hojas_fk0` (`propietario`);

--
-- Indices de la tabla `listaArmas`
--
ALTER TABLE `listaArmas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaArmas_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaEquipo`
--
ALTER TABLE `listaEquipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaEquipo_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraAgilidad`
--
ALTER TABLE `listaExtraAgilidad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraAgilidad_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraComunicacion`
--
ALTER TABLE `listaExtraComunicacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraComunicacion_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraConocimientoFabricacion`
--
ALTER TABLE `listaExtraConocimientoFabricacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraConocimiento_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraConocimientoLeer`
--
ALTER TABLE `listaExtraConocimientoLeer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraConocimiento_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraMagia`
--
ALTER TABLE `listaExtraMagia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraMagia_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraManipulacion`
--
ALTER TABLE `listaExtraManipulacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraManipulacion_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraPercepcion`
--
ALTER TABLE `listaExtraPercepcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraPercepcion_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `listaExtraSigilo`
--
ALTER TABLE `listaExtraSigilo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `listaExtraSigilo_fk0` (`hojaRelacionada`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- Indices de la tabla `magia`
--
ALTER TABLE `magia`
  ADD UNIQUE KEY `hojaRelacionada` (`hojaRelacionada`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aguantePersonaje`
--
ALTER TABLE `aguantePersonaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `hojas`
--
ALTER TABLE `hojas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaArmas`
--
ALTER TABLE `listaArmas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaEquipo`
--
ALTER TABLE `listaEquipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraAgilidad`
--
ALTER TABLE `listaExtraAgilidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraComunicacion`
--
ALTER TABLE `listaExtraComunicacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraConocimientoFabricacion`
--
ALTER TABLE `listaExtraConocimientoFabricacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraConocimientoLeer`
--
ALTER TABLE `listaExtraConocimientoLeer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraMagia`
--
ALTER TABLE `listaExtraMagia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraManipulacion`
--
ALTER TABLE `listaExtraManipulacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraPercepcion`
--
ALTER TABLE `listaExtraPercepcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `listaExtraSigilo`
--
ALTER TABLE `listaExtraSigilo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aguantePersonaje`
--
ALTER TABLE `aguantePersonaje`
  ADD CONSTRAINT `aguantePersonaje_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesAgilidad`
--
ALTER TABLE `habilidadesAgilidad`
  ADD CONSTRAINT `habilidadesAgilidad_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesComunicacion`
--
ALTER TABLE `habilidadesComunicacion`
  ADD CONSTRAINT `habilidadesComunicacion_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesConocimiento`
--
ALTER TABLE `habilidadesConocimiento`
  ADD CONSTRAINT `habilidadesConocimiento_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesManipulacion`
--
ALTER TABLE `habilidadesManipulacion`
  ADD CONSTRAINT `habilidadesManipulacion_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesPercepcion`
--
ALTER TABLE `habilidadesPercepcion`
  ADD CONSTRAINT `habilidadesPercepcion_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `habilidadesSigilo`
--
ALTER TABLE `habilidadesSigilo`
  ADD CONSTRAINT `habilidadesSigilo_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `hojas`
--
ALTER TABLE `hojas`
  ADD CONSTRAINT `hojas_fk0` FOREIGN KEY (`propietario`) REFERENCES `usuarios` (`user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaArmas`
--
ALTER TABLE `listaArmas`
  ADD CONSTRAINT `listaArmas_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaEquipo`
--
ALTER TABLE `listaEquipo`
  ADD CONSTRAINT `listaEquipo_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraAgilidad`
--
ALTER TABLE `listaExtraAgilidad`
  ADD CONSTRAINT `listaExtraAgilidad_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraComunicacion`
--
ALTER TABLE `listaExtraComunicacion`
  ADD CONSTRAINT `listaExtraComunicacion_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraConocimientoFabricacion`
--
ALTER TABLE `listaExtraConocimientoFabricacion`
  ADD CONSTRAINT `listaExtraConocimiento_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraConocimientoLeer`
--
ALTER TABLE `listaExtraConocimientoLeer`
  ADD CONSTRAINT `listaExtraConocimientoLeer_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraMagia`
--
ALTER TABLE `listaExtraMagia`
  ADD CONSTRAINT `listaExtraMagia_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraManipulacion`
--
ALTER TABLE `listaExtraManipulacion`
  ADD CONSTRAINT `listaExtraManipulacion_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraPercepcion`
--
ALTER TABLE `listaExtraPercepcion`
  ADD CONSTRAINT `listaExtraPercepcion_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `listaExtraSigilo`
--
ALTER TABLE `listaExtraSigilo`
  ADD CONSTRAINT `listaExtraSigilo_fk0` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 
--
-- Filtros para la tabla `magia`
--
ALTER TABLE `magia`
  ADD CONSTRAINT `magia_ibfk_1` FOREIGN KEY (`hojaRelacionada`) REFERENCES `hojas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;